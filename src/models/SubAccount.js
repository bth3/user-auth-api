const mongoose = require('mongoose')
const Schema = mongoose.Schema

const subAccountSchema = new Schema({
    subAccountId: {
        type: String,
        required: true,
    },
    user: {
        type: String,
        required: true,
        // type: mongoose.Schema.Types.ObjectId,
        // ref: 'User',
    },

    createdDate: { type: Date, default: Date.now() },
})

module.exports = mongoose.model('SubAccount', subAccountSchema)
