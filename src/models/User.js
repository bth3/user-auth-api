const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs')

const personalInfoSchema = new Schema({
    name: { type: String, required: true },
})

const identitySchema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    verified: { type: Boolean, default: false },
    token: { type: String, required: false },
    passwordResetToken: { type: String, required: false },
    passwordResetExpiration: { type: Date, required: false },
})

const userSchema = new Schema({
    person: personalInfoSchema,
    identity: identitySchema,
    createdDate: { type: Date, default: Date.now() },
})

userSchema.pre('save', async function (next) {
    const hashPassword = await bcrypt.hash(this.identity.password, 10)
    this.identity.password = hashPassword
    next()
})

module.exports = mongoose.model('User', userSchema)
