const express = require('express')
const router = express.Router()

const tokenValidator = require('../src/validator/tokenValidator')
const userController = require('../src/controller/userController')
const loginController = require('../src/controller/loginController')
const mailerController = require('../src/controller/mailerController')
const subAccountController = require('../src/controller/subAccountController')

router
    .route('/users')
    .get(userController.getAllUsers)
    .post(userController.createUser)
router
    .route('/confirmation')
    .get(mailerController.userConfirmVerificationRequest)
router.route('/login').post(loginController.loginConfirm)
router
    .route('/account')
    .post(tokenValidator.regularUserAuth, subAccountController.createSubAccount)
    .get(tokenValidator.regularUserAuth, subAccountController.listSubAccount)
    .put(tokenValidator.regularUserAuth, subAccountController.updateSubAccount)
router
    .route('/balance')
    .post(
        tokenValidator.regularUserAuth,
        subAccountController.balanceSubAccount
    )
module.exports = router
