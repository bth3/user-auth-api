const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/User')

const loginConfirm = async (body) => {
    const user = await User.findOne({ 'identity.email': body.email })
    if (!user) {
        throw {
            statusCode: 401,
            message: 'E-mail ou senha incorreto.',
        }
    }

    const validPassword = await bcrypt.compare(
        body.password,
        user.identity.password
    )
    if (!validPassword) {
        throw {
            statusCode: 403,
            message: 'Senha incorreta.',
        }
    }

    const validConfirmation = user.identity.verified
    if (!validConfirmation) {
        throw {
            statusCode: 403,
            message:
                'Usuário não confirmado. Favor verificar sua caixa de e-mail.',
        }
    }

    //Password remove to return object
    const userToObject = user.toObject()
    delete userToObject.identity.password

    const token = jwt.sign({ data: userToObject }, process.env.JWT_SECRET, {
        expiresIn: 3000, //50min
    })
    return token
}

module.exports = { loginConfirm }
