const crypto = require('crypto')
const User = require('../models/User')

const mailerService = require('../service/mailerService')

const createUser = async (body) => {
    const checkUserExists = await User.findOne({ 'identity.email': body.email })
    if (checkUserExists) {
        throw {
            statusCode: 409, //conflict
            message: 'Usuário já cadastrado!',
        }
    }

    const token = crypto.randomBytes(20).toString('hex')
    try {
        const user = new User({
            person: {
                name: body.name,
            },
            identity: {
                email: body.email,
                password: body.password,
                token: token,
            },
        })
        const userPersistence = await user.save()
        // mailService
        mailerService.sendVerificationRequest(
            userPersistence.identity.email,
            userPersistence.identity.token
        )
        return userPersistence
    } catch (err) {
        console.log(JSON.stringify(err))
        throw {
            statusCode: 400,
            message: 'Não foi possível cadastrar o usuário. Tente novamente.',
        }
    }
}

const userConfirmVerificationRequest = async (code) => {
    try {
        const verifiedUser = await User.findOneAndUpdate(
            { 'identity.token': code },
            {
                $set: {
                    'identity.verified': true,
                },
            },
            { new: true, useFindAndModify: false }
        )
        return verifiedUser.identity.email
    } catch (err) {
        console.log(JSON.stringify(err))
        throw {
            statusCode: 400,
            message: 'Não foi possível realizar a confirmação do usuário.',
        }
    }
}

const getAllUsers = async () => {
    try {
        const users = await User.find(
            {},
            {
                'identity.password': 0,
                'identity.verified': 0,
                'identity.confirmation': 0,
                __v: 0,
            }
        )
        return users
    } catch (err) {
        throw {
            statusCode: 404,
            message: 'Não foi possível realizar a operação.',
        }
    }
}

module.exports = {
    createUser,
    userConfirmVerificationRequest,
    getAllUsers,
}
