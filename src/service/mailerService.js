const nodemailer = require('nodemailer')

const {
    MAILER_HOST,
    MAILER_PORT,
    MAILER_USER,
    MAILER_PASS,
    MAILER_FROM,
} = process.env

const smtpConfig = {
    host: MAILER_HOST,
    port: MAILER_PORT,
    auth: {
        user: MAILER_USER,
        pass: MAILER_PASS,
    },
}

const transport = nodemailer.createTransport(smtpConfig)

const sendVerificationRequest = async (email, token) => {
    try {
        const sendMail = await transport.sendMail({
            to: email,
            from: MAILER_FROM,
            template: 'confirm',
            context: { token },
            html: `<p>Utilize o token a seguir para confirmar sua conta: ${token}</p>`,
        })
        return sendMail
    } catch (err) {
        console.log(JSON.stringify(err))
        throw {
            statusCode: 400,
            message:
                'Não foi possível enviar o e-mail com token de confirmação.',
        }
    }
}

module.exports = {
    sendVerificationRequest,
}
