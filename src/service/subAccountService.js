const qs = require('qs')
const axios = require('axios')
const SubAccount = require('../models/SubAccount')
const tokenDecoder = require('../validator/tokenValidator')

// Environment Variables
const key = process.env.API_RAS_KEY
const secret = process.env.API_RAS_SECRET
const baseUrl = process.env.API_RAS_BASE_URL

const axiosInstance = axios.create({
    baseURL: baseUrl,
    headers: {
        Key: key,
        Secret: secret,
        'content-type': 'application/x-www-form-urlencoded',
    },
})

const createSubAccount = async (body, token) => {
    const accountData = qs.stringify(body)
    const options = {
        method: 'post',
        url: '/criar_subconta',
        data: accountData,
    }
    const tokenDecoded = await tokenDecoder.jwtVerifyPromise(token)
    const userId = tokenDecoded.data._id
    try {
        const response = await axiosInstance(options)
        if (response.data.COD === '01') {
            const account = new SubAccount({
                subAccountId: response.data.ID,
                user: userId,
            })
            await account.save()
            return response.data
        }
        throw response.data
    } catch (err) {
        throw {
            statusCode: 500,
            api: {
                cod: err.COD,
                message: err.Mensagem,
            },
        }
    }
}

const listSubAccount = async () => {
    try {
        const response = await axiosInstance({
            method: 'get',
            url: '/listar_subconta',
        })
        if (response.data.COD === '01') {
            return response.data
        }
        throw response.data
    } catch (err) {
        throw {
            statusCode: 500,
            api: {
                cod: err.COD,
                message: err.Mensagem,
            },
        }
    }
}

const balanceSubAccount = async (id) => {
    try {
        const response = await axiosInstance({
            method: 'post',
            data: qs.stringify({
                subconta: id,
            }),
            url: '/exibir_balanco_subconta',
        })
        if (response.data.COD === '01') {
            return response.data
        }
        throw response.data
    } catch (err) {
        throw {
            statusCode: 500,
            api: {
                cod: err.COD,
                message: err.Mensagem,
            },
        }
    }
}

const updateSubAccount = async (body) => {
    const dataAccount = qs.stringify(body)
    try {
        const response = await axiosInstance({
            method: 'post',
            data: dataAccount,
            url: '/atualizar_dados_subconta',
        })
        if (response.data.COD === '01') {
            return response.data
        }
        throw response.data
    } catch (err) {
        throw {
            statusCode: 500,
            api: {
                cod: err.COD,
                message: err.Mensagem,
            },
        }
    }
}

module.exports = {
    createSubAccount,
    listSubAccount,
    balanceSubAccount,
    updateSubAccount,
}
