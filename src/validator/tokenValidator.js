const jwt = require('jsonwebtoken')

const regularUserAuth = async (req, res, next) => {
    const userToken = req.headers.authorization
    if (!userToken) {
        return res.status(401).json({
            message: 'Falta de token.',
        })
    }
    try {
        await jwtVerifyPromise(userToken)
    } catch (err) {
        console.log(JSON.stringify(err))
        return res.status(403).json({
            message: 'Token inválido.',
        })
    }
    next()
}

const jwtVerifyPromise = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return reject(err)
            }
            return resolve(decoded)
        })
    })
}

module.exports = { regularUserAuth, jwtVerifyPromise }
