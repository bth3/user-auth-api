require('dotenv').config()
const mongoose = require('mongoose')

const { DB_USER, DB_PASS, DB_NAME, DB_HOST } = process.env

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

const url = `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_NAME}?retryWrites=true&w=majority`

mongoose.connect(url, options)
mongoose.connection.on('error', () =>
    console.error('MongoDB connection error:')
)
mongoose.connection.once('open', () =>
    console.log('MongoDB connected successfully!')
)
