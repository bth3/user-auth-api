const userService = require('../service/userService')

const userConfirmVerificationRequest = async (req, res) => {
    try {
        const user = await userService.userConfirmVerificationRequest(
            req.query.code
        )
        return res.status(200).json({
            code: req.query.code,
            email: user,
            message: 'Confirmação realizada com sucesso!',
        })
    } catch (err) {
        console.log(JSON.stringify(err))
        return res.status(err.statusCode).json({
            message: err.message,
        })
    }
}

module.exports = {
    userConfirmVerificationRequest,
}
