const subAccountService = require('../service/subAccountService')

const createSubAccount = async (req, res) => {
    try {
        const account = await req.body
        const token = await req.headers.authorization
        const create = await subAccountService.createSubAccount(account, token)
        return res.status(201).json({
            data: {
                COD: create.COD,
                ID: create.ID,
                Mensagem: create.Mensagem,
            },
        })
    } catch (err) {
        return res.status(err.statusCode).json({
            errorCode: err.api.cod,
            message: err.api.message,
        })
    }
}

const listSubAccount = async (req, res) => {
    try {
        const listOfAll = await subAccountService.listSubAccount()
        return res.status(200).json(listOfAll)
    } catch (err) {
        return res.status(err.statusCode).json({
            errorCode: err.api.cod,
            message: err.api.message,
        })
    }
}

const balanceSubAccount = async (req, res) => {
    try {
        const subAccountId = req.body.subconta
        const balance = await subAccountService.balanceSubAccount(subAccountId)
        return res.status(200).json(balance)
    } catch (err) {
        return res.status(err.statusCode).json({
            errorCode: err.api.cod,
            message: err.api.message,
        })
    }
}

const updateSubAccount = async (req, res) => {
    try {
        const data = await req.body
        const response = await subAccountService.updateSubAccount(data)
        return res.status(200).json({
            data: {
                COD: response.COD,
                ID: response.ID,
                Mensagem: response.Mensagem,
            },
        })
    } catch (err) {
        return res.status(err.statusCode).json({
            errorCode: err.api.cod,
            message: err.api.message,
        })
    }
}

module.exports = {
    createSubAccount,
    listSubAccount,
    balanceSubAccount,
    updateSubAccount,
}
