const loginService = require('../service/loginService')

const loginConfirm = async (req, res) => {
    try {
        const token = await loginService.loginConfirm(req.body)
        return res.json({
            token: token,
        })
    } catch (err) {
        res.status(err.statusCode).json({
            message: err.message,
        })
    }
}

module.exports = { loginConfirm }
