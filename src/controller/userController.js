const userService = require('../service/userService')

const createUser = async (req, res) => {
    try {
        const user = await userService.createUser(req.body)
        return res.status(201).json({
            data: {
                user_id: user._id,
                person: {
                    name: user.person.name,
                },
                identity: {
                    email: user.identity.email,
                },
            },
            message:
                'Usuário cadastrado com sucesso. Confirmação enviada para o endereço de e-mail informado.',
        })
    } catch (err) {
        return res.status(err.statusCode).json({
            message: err.message,
        })
    }
}

const getAllUsers = async (req, res) => {
    try {
        const users = await userService.getAllUsers()
        return res.status(200).json(users)
    } catch (err) {
        return res.status(err.statusCode).json({
            message: err.message,
        })
    }
}

module.exports = {
    createUser,
    getAllUsers,
}
