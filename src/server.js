require('dotenv').config()
const express = require('express')
const app = express()
const db = require('./database/connection')
const router = require('./router')
const bodyParser = require('body-parser')

app.use(express.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/api', router)
app.use(express.static('public'))

const PORT = process.env.SERVER_PORT
app.listen(PORT, () => console.log(`Server listening on port ${PORT}...`))
